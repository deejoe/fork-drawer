
[LibreSprite](https://libresprite.github.io/#!/about) 
was forked from the last GPL version of Aseprite when 
the latter moved  
[to a non-free license](https://github.com/aseprite/aseprite/commit/5ecc356a41c8e29977f8608d8826489d24f5fa6c) 
for their efforts, starting in late August 2016, after version 1.1.8.
