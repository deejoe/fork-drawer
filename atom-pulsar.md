
Github announced on June 8, 2022 that they would [sunset the Atom text editor](10) starting December 15, 2022.

That inspired the creation of a text editor built on the cross-platform, browser-based runtime
Electron, called [Pulsar](https://pulsar-edit.dev/), as a fork of Atom.

[10]: https://github.blog/2022-06-08-sunsetting-atom/
[20]: https://pulsar-edit.dev/

