
As per the [Wikipedia node.js article][10]:

    In December 2014, Fedor Indutny started io.js, a fork of Node.js. Due to the internal conflict over Joyent's governance, io.js was created as an open governance alternative with a separate technical committee.

    In September 2015, Node.js v0.12 and io.js v3.3 were merged back together into Node v4.0.

The Node.js Foundation was formed to support development, later merging with the JS Foundation [to form the OpenJS Foundation][30].

[10]: https://en.wikipedia.org/wiki/Node.js
[20]: https://nodejs.org/en/download/releases/
[30]: https://en.wikipedia.org/wiki/OpenJS_Foundation
