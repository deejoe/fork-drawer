

This [Font-Awesome GitHub issue](https://github.com/FortAwesome/Font-Awesome/issues/12199#issuecomment-362919956) 
[(archive)](https://web.archive.org/save/https://github.com/FortAwesome/Font-Awesome/issues/12199)
highlights the changes that led to the establishment of a fork under the
[Fork-Awesome](https://forkawesome.github.io/Fork-Awesome/)
banner.

