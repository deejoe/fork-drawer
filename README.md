
This is a collection of software project forks, collected primarily to
exemplify the assertion of software freedoms.  

The term 'fork' here is used in the older, pre-GitHub sense of 
[a division of community development effort into two distinct projects](https://opensource.com/article/17/12/fork-clone-difference), 
rather than the trivial sense of a clone or branch of a source code corpus
used in the normal course of development and testing of those working with
or within a project.

In the simplest form, it should make apparent any child-parent relationships
amongst projects

Stretch goals include annotations indicating the date of the fork, the
nature of the fork, any divergence in goals or features of the child
projects, languages used, development environments, documention for the
projects or for the motivations for entering the forking process, post-fork
disposition of the child projects, influence on other projects,
characterization of cooperation between or among child projects after the
fork including any later merging of the projects.

Merges of distinctly-originated projects will also be considered. If so, the
focus is on code bases rather than on [companies](https://ourincrediblejourney.tumblr.com/).

