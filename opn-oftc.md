
In 2001, the [Open and Free Technology Community (OFTC)][10] [IRC][20]
network was formed by those dissatisfied with what was offered for the free
software community by the Open Projects Network (OPN, later renamed to
[Freenode][30]).

It aspired to [offer services beyond IRC][40], as something that
differentiated it from OPN.  Those plans were never realized.  Due perhaps
to the attention other IRC networks received during the Freenode management
change in 2021, [reference to those plans][50] was [ dropped][60] in late
May 2021.

[10]: https://en.wikipedia.org/wiki/Open_and_Free_Technology_Community
[20]: https://en.wikipedia.org/wiki/Internet_Relay_Chat
[30]: https://en.wikipedia.org/wiki/Freenode
[40]: https://web.archive.org/web/20021013174738/http://www.oftc.net/faq/oftc/
[50]: https://web.archive.org/web/20210304090311/https://www.oftc.net/FAQ/General_Questions/#what-services-does-oftc-provide
[60]: https://www.oftc.net/FAQ/General_Questions/#what-services-does-oftc-provide

