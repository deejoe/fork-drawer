
Those previously developing the [Amara][10] subtitling suite under a [FOSS license][20] [announced][30] they would continue development under a proprietary license.  See also [Slashdot][40].


[10]: https://en.wikipedia.org/wiki/Amara_%28subtitling%29

[20]: https://gitlab.com/hanklank/amara-archive

[30]: https://blog.amara.org/2020/01/13/why-we-are-closing-amaras-source-code/

[40]: https://news.slashdot.org/story/20/01/18/1753246/another-project-goes-private-amara-stops-being-developed-as-open-source

