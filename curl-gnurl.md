
OK, so this one just came to my attention, via a brief item on the Planet
GNU aggregate feed:

[~~gnURL 7.56.0 released~~ dead link](https://gnunet.org/node/2657)
Sun, 10/08/2017 - 17:25 — ng0

I thought, "what is this *gnurl* thing, anyway" and poked around to find:

[libgnurl](https://gnunet.org/en/gnurl.html)
Wed, 10/23/2013 - 09:13 — Christian Grothoff

So, I'm reading along, nodding my head and thinking "ok, this seems fine,
it doesn't seem to have any of the rancor and vituperation about upstream
being unresponsive or greedy mountebanks or whatever". 

Then I dig around a little more to find:

cURL and libcurl, Open Source
[he forked off libgnurl](https://daniel.haxx.se/blog/2013/10/27/he-forked-off-libgnurl/)
October 27, 2013 Daniel Stenberg	

and see that it might not have been received with such affection from the
parent project.

So it goes.

