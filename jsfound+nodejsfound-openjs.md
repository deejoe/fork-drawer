
The [Node.js Foundation][10] and the [JS Foundation][20] voted March 2019 to merge into the 
[OpenJS Foundation][30] under the auspices of the [Linux Foundation][40].

[10]: https://web.archive.org/web/20191105225747/https://foundation.nodejs.org/
[20]: https://web.archive.org/web/20191104144133/https://js.foundation/
[30]: https://openjsf.org/
[40]: https://en.wikipedia.org/wiki/Linux_Foundation

