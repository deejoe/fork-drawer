
This collection is copyright D. Joe Anderson 2017-2019.

All content in this collection by D. Joe Anderson is released for use
under the [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html)
license.

Other contributions are copyright their original authors using the indicated
licenses where different from the collection as a whole.

