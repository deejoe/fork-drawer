[Ubuntu][10] began as a fork of [Debian][20] in October 2004, between the
releases of [Debian 3.0 (woody)][30] and [Debian 3.1 (sarge)][40]

It has not been a hard fork, however, as Ubuntu continues to draw from
Debian and the two projects have some level of cooperation between them.

[10]: https://en.wikipedia.org/wiki/Ubuntu 
[20]: https://en.wikipedia.org/wiki/Debian
[30]: https://en.wikipedia.org/wiki/Debian_version_history#Debian_3.0_(Woody)
[40]: https://en.wikipedia.org/wiki/Debian_version_history#Debian_3.1_(Sarge)

