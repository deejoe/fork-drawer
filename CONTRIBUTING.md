
Please feel free to put a fork in the [fork drawer](https://gitlab.com/deejoe/fork-drawer). 

[Free software](https://www.fsf.org) projects only, though, please.

To do so, please clone/fork this repo, do the necessary, and then [submit a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) for your entry.

For now, I think a simple ```parent-child``` naming scheme for entries is
enough. Might be fun to make an index or table or something eventually. If
a project name includes the ```-``` character, then use an underscore,
```_``` to delimit the two projects please.



Tell us who forked what, when.  Give the name of the parent project, and of
the child project.  Please provide some links--if it's an old fork, a
[Wikipedia](https://en.wikipedia.org/wiki/List_of_software_forks) entry (perhaps for a specific date) may be enough--we don't need
to re-invent the wheel __entirely__.  If not, links to blog posts or mailing lists
entries or About pages or readme files are fine.  It would be especially
diligent and appreciated to make sure [The Wayback Machine](https://archive.org) has a copy of whatever it is.

Merges are also interesting here (for our purposes we'll just
think of them as a special kind of fork--a reverse fork) so feel free to
include those. If the successor projects of a merge take on a new name, then
list any constituent predecessor project names first and the combined
project name last. If the merge is the later evolution of an earlier fork,
with development continuing on under the earlier name, just include that in
the write-up of the original fork.

Please be sure to include a suitable license with your submitted entry.

I'll be using 

[CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html)

but any CC license that doesn't include the NC or ND clauses should be fine.

Please put a copyright and license statement (an [SPDX tag](https://spdx.org/) would be fine) either in your entry or add it
to the [LICENSE.md](LICENSE.md) file.

I haven't given workflow or branching models a lot of thought for this just
yet, given that this is so far nice and casual and has only one entry. 

With suitable licensing and attestation we can cross that bridge if we come
to it.

Also, I don't have much in the way of Codes of Conduct. For now let me just
say: Be cool. The point of this is to collect and report the facts of there
having been a fork for any given instance. 

Characterizing the timing and nature of the fork is what we're after beyond
that. If we have to choose between "the fork resulted from so-and-so
being a real jerk" or just a very simple and barely-informative "so, that
happened" we definitely want the latter.

It would be nice to include some indication as to the impact of the software
in question. A light and breezy style, free from significant
[attachment](https://en.wikipedia.org/wiki/Up%C4%81d%C4%81na), would be
nice, if we can manage it.  Heavy editorializing and opinionated
judgmentalism about how it all went down is for a different project. 

If we've done our work here well enough, people who want details of who said
what, and how far whose veins were popping out when it was said, they should
be able to start from the links we've provided and see for themselves.

