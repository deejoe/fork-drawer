
[Fossify][05]

Most of the Simple Mobile Tools [Android applications suite were forked][10] by
[the Fossify project][20] when Simple Mobile Tools was sold by their maintainer to
ZipoApps.

This happened right around [December 2023][30]. There was concern that ZipoApps
would insert ads and other forms of monetized friction to the user the
experience and would [no longer be developed as open source software][40].

[05]: https://www.fossify.org/about/

[10]: https://github.com/SimpleMobileTools/General-Discussion/issues/241#issuecomment-1837102917 

[20]: https://github.com/FossifyOrg

[30]: https://liliputing.com/fossify-apps-pick-up-where-simple-mobile-tools-left-off-free-and-open-source-suite-of-android-apps/

[40]: https://liliputing.com/android-app-maker-simple-mobile-tools-acquired-by-zipoapps/




