
The media system [jellyfin][10] was [forked from emby in December 2018][20] soon after
the latter project [moved towards proprietary development.][25]

Upstream source code releases ended with version [3.5][30] and continued in
binary-only form via [a different repository.][40]

[10]: https://jellyfin.org/
[20]: https://jellyfin.org/docs/general/about.html
[25]: https://web.archive.org/web/20181212104719/https://github.com/MediaBrowser/Emby/issues/3479
[30]: https://github.com/MediaBrowser/Emby/releases
[40]: https://github.com/MediaBrowser/Emby.Releases
