
**htop** had grown stale, with unapplied patches accumulating over a span of a couple years.

So, in the summer of 2020, a group calling itself [htop-dev][10] [forked it, applied patches, and announced their version.][20]

[10]: https://htop.dev
[20]: https://groups.io/g/htop/topic/htop_3_0_0_released/76441967

