[Rocky][10] [Linux][20] was founded to continue with the original aims of
[CentOS][30] after Red
Hat announced [a shift][40] [in the use][50] [of the CentOS name][60] 
[for other purposes][70].

[10]: https://en.wikipedia.org/wiki/Rocky_Linux

[20]: https://rockylinux.org/

[30]: https://en.wikipedia.org/wiki/CentOS

[40]: https://www.theregister.com/2021/01/26/killing_centos/

[50]: https://arstechnica.com/gadgets/2020/12/centos-shifts-from-red-hat-unbranded-to-red-hat-beta/

[60]: https://arstechnica.com/gadgets/2021/01/on-the-death-of-centos-red-hat-liaison-brian-exelbierd-speaks/

[70]: https://arstechnica.com/gadgets/2020/12/centos-linux-is-gone-but-its-refugees-have-alternatives/

