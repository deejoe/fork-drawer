

[Backdrop CMS](https://github.com/backdrop/backdrop)
is a fork of 
[Drupal](https://cgit.drupalcode.org/drupal).

This according to the README.md currently in the backdrop Github repo above.

No details there as to who did the fork, when, and the why seems to be over
architectural philosophy?

Came to our attention from the syndicated lead blurb for Software Freedom
Conservancy's 
[announcement that Backdrop CMS joined Conservancy](https://sfconservancy.org/news/2018/may/08/backdropjoinsSFC/)

That article mentions one "who" at least: Jen Lampton.

