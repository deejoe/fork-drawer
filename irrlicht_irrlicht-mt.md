
Due to upstream not having a release in several years, the Minetest
developers announced in April 2021 they would be maintaining their own fork
of the Irrlicht game engine.

[10] https://minetest.net

[20] https://forum.minetest.net/viewtopic.php?t=26511

[30] https://github.com/minetest/irrlicht

[40] https://irrlicht.sourceforge.io/

