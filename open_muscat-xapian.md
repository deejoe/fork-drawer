
The [Xapian][10] information retrieval library was forked circa 2001 from the last
version of [Open Muscat][20] released under the GPL.

[10]: https://en.wikipedia.org/wiki/Xapian
[20]: https://xapian.org/history

