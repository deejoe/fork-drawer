
The radio station management software [Airtime](https://en.wikipedia.org/wiki/Airtime_(software))
was forked as [LibreTime](https://libretime.org/) ([archive](https://web.archive.org/web/20200108175607/https://libretime.org/)) over dissatisfaction with
the pace of contributions being released into the former. 

https://gist.github.com/hairmare/8c03b69c9accc90cfe31fd7e77c3b07d ([archive](https://web.archive.org/web/20190905034510/https://gist.github.com/hairmare/8c03b69c9accc90cfe31fd7e77c3b07d))

https://en.wikipedia.org/wiki/Airtime_(software)#Stalled_Development_and_LibreTime_Fork

