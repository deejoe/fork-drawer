
I think it a stretch to call this a fork, although a more expansive 
conception of "community" might qualify it.

That said, after a series of previous renamings, and as its support for the 
hardware from which it had taken its earlier names, the Xbox, dwindled 
almost to nothing, [XBMC was renamed to Kodi in 2014][rename]. 

[rename]: https://kodi.tv/article/xbmc-getting-new-name-introducing-kodi-14

