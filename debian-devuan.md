
[Devuan][10] was forked from [Debian][20] in 2014[30] over the latter's 
adoption of [systemd][40].

[10]: https://www.devuan.org/
[20]: https://www.debian.org/
[30]: https://www.phoronix.com/scan.php?page=news_item&px=MTg1MDQ
[40]: https://systemd.io/

