
The Glimpse Image Editor was forked from the GNU Image Manipulation Program in 2019, largely but not exclusively motivated by the perception that the acronym for the latter has avoidably unsavory associations.

From an earlier version of:

https://glimpse-editor.org/about/#why-are-you-forking

> Why are you forking?
>
> We do not believe that upstream’s suggestion to “expand the acronym” to fix their software’s name adequately addresses the problem, and after 13+ years of users repeatedly asking we think the most constructive path forward is to fork the project.
>
> In addition, the dedicated UI design team for GNU Image Manipulation Program stopped meeting in 2012. While development work on the GUI and installers still continues today, we felt as end users that a higher priority had been given to more specialized functionality and code restructuring. We want to focus solely on improving the overall user experience and packaging the application for a new audience that may not have heard about free software yet.
>
> Finally there was a historic decision to remove the third party plug-in registry and in-built mechanism that supported it. While we lack the resources to reinstate that system, we do plan to provide additional install options that pre-bundle a selection of useful third party plug-ins.

https://web.archive.org/web/20200404134849/https://glimpse-editor.org/about/#why-are-you-forking

