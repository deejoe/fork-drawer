
The CodiMD collaborative online Markdown editor [changed its name][10] to HedgeDoc
in 2020. 

CodiMD had, itself, resulted from a fork of HackMD CE (presumably CE for
*community edition* as there was also a HackMD EE (presumably *enterprise
edition*).

[10]: https://hedgedoc.org/history/

