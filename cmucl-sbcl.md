
> In December 1999, [SBCL](http://www.sbcl.org/history.html) forked off the main branch of [CMUCL](https://cmucl.org/).

According to Wikipedia, [SBCL's fork from CMUCL](https://en.wikipedia.org/wiki/Steel_Bank_Common_Lisp#History) has been "amicable".

