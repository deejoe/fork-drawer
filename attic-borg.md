
The Borg Collective forked Jonas Borgström's [attic][10] deduplicating 
backup program [in 2015][20], somewhat archly calling their new project [borg][30] 

[10]: https://attic-backup.org/
[20]: https://en.wikipedia.org/wiki/Attic_(backup_software)#Borg
[30]: https://www.borgbackup.org/

