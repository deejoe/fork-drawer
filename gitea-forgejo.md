
The git-based code hosting software [Forgejo][10] was [forked][20] in 2022 from the [Gitea][30] project.

Both projects continue.

[10]: https://forgejo.org/
[20]: https://forgejo.org/compare/#why-was-forgejo-created
[30]: https://gitea.io/

