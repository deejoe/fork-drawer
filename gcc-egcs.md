
The Gnu Compiler Collection (gcc) version 2.7.2 was [forked][10] in 1997 as the Experimental/Enhanced GNU Compiler System
(EGCS). By April 1999 EGCS development supplanted gcc development, and EGCS
became gcc 2.95 in July 1999.

[10]: https://en.wikipedia.org/w/index.php?title=GNU_Compiler_Collection&oldid=1002917167#EGCS_fork

