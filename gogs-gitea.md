
The git-based code hosting software written in Go, [Gogs](https://gogs.io/),
was [forked](https://github.com/gogits/gogs/issues/1304) in 2015 
to form the [Gitea](<https://gitea.io/>) project.

Both projects continue.
