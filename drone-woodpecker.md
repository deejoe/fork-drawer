
The [Woodpecker Continuous Integration (CI) engine][10] was forked from the Drone project at ["version 0.8, right before the 1.0 release and license changes"][20].
 
The [license change][50] happened in February 2018.

The last release of Drone prior to that date was [0.8.4][60]

[10]: https://woodpecker.laszlo.cloud/
[20]: https://github.com/laszlocph/woodpecker/ 
[50]: https://github.com/drone/drone/commit/ce3855812bb00dbf1993d8663b6100b272e104dd#diff-c693279643b8cd5d248172d9c22cb7cf4ed163a3c98c8a3f69c2717edd3eacb7
[60]: https://github.com/drone/drone/releases/tag/v0.8.4

